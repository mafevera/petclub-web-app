# PETCLUB WEB APP
<img src="login.png" style="width:1000px;">
<b>Presentado por:</b><br/>
Maria Fernanda Vera<br/>
Andrea Fabiana Villamizar<br/>
Anngy Nathalia Gomez<br/>
Martha Eliana Arenas<br/>
Carlos Daniel Barrera<br/><br/>

<b>Asignatura:</b><br/>
Ingeniería del Software II

Ingeniería de Sistemas.<br/>
Universidad Industrial de Santander

---
[1] El código del proyecto se encuentra en la carpeta <a href="https://gitlab.com/mafevera/petclub-web-app/-/tree/master/code" rel="nofollow noreferrer noopener" target="_blank">code</a><br/>

[2]<a href="https://gitlab.com/mafevera/petclub-web-app/-/blob/master/Documentos/Poster-PETCLUB.pdf" rel="nofollow noreferrer noopener" target="_blank"> Poster</a><br/>

[3]<a href="https://gitlab.com/mafevera/petclub-web-app/-/blob/master/Documentos/Copia%20de%20plantilla-veterinaria.pptx" rel="nofollow noreferrer noopener" target="_blank"> Presentación</a><br/>

[4]<a href="https://www.youtube.com/watch?v=ZLeyQNL91ds&feature=youtu.be" rel="nofollow noreferrer noopener" target="_blank">Video </a>corto (4 minutos)<br/>

[5]<a href="https://www.youtube.com/watch?v=A74F425TnLg&feature=youtu.be" rel="nofollow noreferrer noopener" target="_blank">Video</a> con la presentación completa (15 minutos)<br/>



